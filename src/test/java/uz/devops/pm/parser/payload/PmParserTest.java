package uz.devops.pm.parser.payload;

import org.junit.Test;
import uz.devops.pm.parser.payload.enumeration.MethodType;
import uz.devops.pm.parser.payload.enumeration.ModeType;
import uz.devops.pm.parser.payload.enumeration.ProtocolType;
import uz.devops.pm.parser.payload.enumeration.RequestType;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class PmParserTest {

    private static final String SAMPLE_URL = "devops.uz";
    private static final RequestType SAMPLE_TYPE = RequestType.CHECK;
    private static final String SAMPLE_USERNAME = "admin";
    private static final String SAMPLE_PASSWORD = "admin";
    private static final String SAMPLE_TOKEN = "token";
    private static final String SAMPLE_BODY = "body";
    private static final String SAMPLE_STATUS_QUERY = "status_query";
    private static final String SAMPLE_MESSAGE_QUERY = "message_query";
    private static final String SAMPLE_SUCCESS_CODE = "200";
    private static final String SAMPLE_CALLBACK = "callback";
    private static final Integer SAMPLE_TIMEOUT = 0;
    private static final String SAMPLE_HEADERS = "headers";
    private static final String SAMPLE_DATA_QUERIES = "data_queries";
    private static final MethodType SAMPLE_METHOD_TYPE = MethodType.PATCH;
    private static final ProtocolType SAMPLE_PROTOCOL = ProtocolType.REST;
    private static final String SAMPLE_REQUEST_MEDIA_TYPE = "application/json";
    private static final String SAMPLE_RESPONSE_MEDIA_TYPE = "application/json";
    private static final boolean SAMPLE_ACTIVE = false;
    private static final ModeType SAMPLE_MODE = ModeType.TEST;
    private static final String SAMPLE_EXT_ID_QUERY = "ext_id_query";
    private static final boolean SAMPLE_SUCCESS_ON_2XX = true;

    @Test
    public void testStateIsCorrect() {
        PmParser pmParser = new PmParser(
            SAMPLE_URL,
            SAMPLE_TYPE,
            SAMPLE_USERNAME,
            SAMPLE_PASSWORD,
            SAMPLE_TOKEN,
            SAMPLE_BODY,
            SAMPLE_STATUS_QUERY,
            SAMPLE_MESSAGE_QUERY,
            SAMPLE_SUCCESS_CODE,
            SAMPLE_CALLBACK,
            SAMPLE_TIMEOUT,
            SAMPLE_HEADERS,
            SAMPLE_DATA_QUERIES,
            SAMPLE_METHOD_TYPE,
            SAMPLE_PROTOCOL,
            SAMPLE_REQUEST_MEDIA_TYPE,
            SAMPLE_RESPONSE_MEDIA_TYPE,
            SAMPLE_ACTIVE,
            SAMPLE_MODE,
            SAMPLE_EXT_ID_QUERY,
            SAMPLE_SUCCESS_ON_2XX
        );

        assertThat(pmParser.getUrl(), equalTo(SAMPLE_URL));
        assertThat(pmParser.getType(), equalTo(SAMPLE_TYPE));
        assertThat(pmParser.getUsername(), equalTo(SAMPLE_USERNAME));
        assertThat(pmParser.getPassword(), equalTo(SAMPLE_PASSWORD));
        assertThat(pmParser.getToken(), equalTo(SAMPLE_TOKEN));
        assertThat(pmParser.getBody(), equalTo(SAMPLE_BODY));
        assertThat(pmParser.getStatusQuery(), equalTo(SAMPLE_STATUS_QUERY));
        assertThat(pmParser.getMessageQuery(), equalTo(SAMPLE_MESSAGE_QUERY));
        assertThat(pmParser.getSuccessCode(), equalTo(SAMPLE_SUCCESS_CODE));
        assertThat(pmParser.getCallback(), equalTo(SAMPLE_CALLBACK));
        assertThat(pmParser.getTimeout(), equalTo(SAMPLE_TIMEOUT));
        assertThat(pmParser.getHeaders(), equalTo(SAMPLE_HEADERS));
        assertThat(pmParser.getDataQueries(), equalTo(SAMPLE_DATA_QUERIES));
        assertThat(pmParser.getHttpMethodType(), equalTo(SAMPLE_METHOD_TYPE));
        assertThat(pmParser.getProtocol(), equalTo(SAMPLE_PROTOCOL));
        assertThat(pmParser.getRequestMediaType(), equalTo(SAMPLE_REQUEST_MEDIA_TYPE));
        assertThat(pmParser.getResponseMediaType(), equalTo(SAMPLE_RESPONSE_MEDIA_TYPE));
        assertThat(pmParser.getActive(), equalTo(SAMPLE_ACTIVE));
        assertThat(pmParser.getMode(), equalTo(SAMPLE_MODE));
        assertThat(pmParser.getExtIdQuery(), equalTo(SAMPLE_EXT_ID_QUERY));
        assertThat(pmParser.getSuccessOn2xx(), equalTo(SAMPLE_SUCCESS_ON_2XX));
    }

    @Test
    public void testEqualsTwoObjectsWithSameValues() {
        PmParser underTest = new PmParser(
            SAMPLE_URL,
            SAMPLE_TYPE,
            SAMPLE_USERNAME,
            SAMPLE_PASSWORD,
            SAMPLE_TOKEN,
            SAMPLE_BODY,
            SAMPLE_STATUS_QUERY,
            SAMPLE_MESSAGE_QUERY,
            SAMPLE_SUCCESS_CODE,
            SAMPLE_CALLBACK,
            SAMPLE_TIMEOUT,
            SAMPLE_HEADERS,
            SAMPLE_DATA_QUERIES,
            SAMPLE_METHOD_TYPE,
            SAMPLE_PROTOCOL,
            SAMPLE_REQUEST_MEDIA_TYPE,
            SAMPLE_RESPONSE_MEDIA_TYPE,
            SAMPLE_ACTIVE,
            SAMPLE_MODE,
            SAMPLE_EXT_ID_QUERY,
            SAMPLE_SUCCESS_ON_2XX
        );
        assertThat(underTest, equalTo(new PmParser(
            SAMPLE_URL,
            SAMPLE_TYPE,
            SAMPLE_USERNAME,
            SAMPLE_PASSWORD,
            SAMPLE_TOKEN,
            SAMPLE_BODY,
            SAMPLE_STATUS_QUERY,
            SAMPLE_MESSAGE_QUERY,
            SAMPLE_SUCCESS_CODE,
            SAMPLE_CALLBACK,
            SAMPLE_TIMEOUT,
            SAMPLE_HEADERS,
            SAMPLE_DATA_QUERIES,
            SAMPLE_METHOD_TYPE,
            SAMPLE_PROTOCOL,
            SAMPLE_REQUEST_MEDIA_TYPE,
            SAMPLE_RESPONSE_MEDIA_TYPE,
            SAMPLE_ACTIVE,
            SAMPLE_MODE,
            SAMPLE_EXT_ID_QUERY,
            SAMPLE_SUCCESS_ON_2XX
        )
        ));
    }

}
