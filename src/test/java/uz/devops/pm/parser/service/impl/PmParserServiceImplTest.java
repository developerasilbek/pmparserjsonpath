package uz.devops.pm.parser.service.impl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;
import uz.devops.pm.parser.config.TestConfig;
import uz.devops.pm.parser.payload.PmParser;
import uz.devops.pm.parser.payload.enumeration.MethodType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TestConfig.class})
public class PmParserServiceImplTest {

    @Autowired
    private PmParserServiceImpl pmParserService;

    private MultipartFile correctFile;
    private MultipartFile wrongFile;

    private static final String SAMPLE_URL = "https://www.devops.uz";
    private static final String WRONG_URL = "https://www.kun.uz";
    private static final String SAMPLE_USERNAME = "admin";
    private static final String WRONG_USERNAME = "user";
    private static final String SAMPLE_PASSWORD = "admin";
    private static final String WRONG_PASSWORD = "123";
    private static final MethodType SAMPLE_METHOD_TYPE = MethodType.GET;
    private static final MethodType WRONG_METHOD_TYPE = MethodType.DELETE;

    @Before
    public void setUp() throws IOException {

        correctFile = getMultipartFile(correctFile, "src/test/resources/TestPostmanCollection.postman_collection.json");
        wrongFile = getMultipartFile(wrongFile, "src/test/resources/TestWrongFile.txt");

    }

    @After
    public void tearDown() {
        //Do something on finishing
    }

    @Test
    public void testCreatePmParserFromFileShouldSucceed() throws IOException {
        List<PmParser> underTest = pmParserService.parse(correctFile);
        assertNotNull(underTest);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getUrl(), SAMPLE_URL);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getUsername(), SAMPLE_USERNAME);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getPassword(), SAMPLE_PASSWORD);
        assertEquals(underTest.stream().findFirst().orElse(new PmParser()).getHttpMethodType(), SAMPLE_METHOD_TYPE);

        System.out.println("Created PmParser : {}\n" + underTest + "\n");
    }

    @Test
    public void testCreatePmParserFromFileShouldFail() throws IOException {
        List<PmParser> underTest = pmParserService.parse(correctFile);
        assertNotNull(underTest);
        assertNotEquals(underTest.stream().findFirst().orElse(new PmParser()).getUrl(), WRONG_URL);
        assertNotEquals(underTest.stream().findFirst().orElse(new PmParser()).getUsername(), WRONG_USERNAME);
        assertNotEquals(underTest.stream().findFirst().orElse(new PmParser()).getPassword(), WRONG_PASSWORD);
        assertNotEquals(underTest.stream().findFirst().orElse(new PmParser()).getHttpMethodType(), WRONG_METHOD_TYPE);
        assertFalse(underTest.stream().findFirst().orElse(new PmParser()).getHeaders().isEmpty());

        System.out.println("Created PmParser : {}\n" + underTest + "\n");
    }

    @Test
    public void testCheckToFileShouldSucceed() {
        boolean underTest = pmParserService.checkFile(correctFile);
        assertTrue(underTest);

        System.out.println("This is Postman Collection File : {}\n" + underTest + "\n");
    }

    @Test
    public void testCheckToFileShouldFail() {
        boolean underTest = pmParserService.checkFile(wrongFile);
        assertFalse(underTest);

        System.out.println("This is not Postman Collection File : {}\n" + underTest + "\n");
    }

    public static MultipartFile getMultipartFile(MultipartFile multipartFile, String path) throws IOException {
        File file = new File(path);
        byte[] bytes = new byte[(int) file.length()];

        try (FileInputStream fis = new FileInputStream(file)) {
            fis.read(bytes);
        }
        multipartFile = new MockMultipartFile("file",
            file.getName(), "text/plain", bytes);
        return multipartFile;
    }
}
