package uz.devops.pm.parser.payload.enumeration;

public enum ProtocolType {
    REST,
    WS,
    HESSIAN,
}
