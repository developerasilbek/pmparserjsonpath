package uz.devops.pm.parser.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.devops.pm.parser.payload.enumeration.MethodType;
import uz.devops.pm.parser.payload.enumeration.ModeType;
import uz.devops.pm.parser.payload.enumeration.ProtocolType;
import uz.devops.pm.parser.payload.enumeration.RequestType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PmParser {
    private String url;

    private RequestType type;

    private String username;

    private String password;

    private String token;

    private String body;

    private String statusQuery;

    private String messageQuery;

    private String successCode;

    private String callback;

    private Integer timeout;

    private String headers;

    private String dataQueries;

    private MethodType httpMethodType;

    private ProtocolType protocol;

    private String requestMediaType;

    private String responseMediaType;

    private Boolean active;

    private ModeType mode;

    private String extIdQuery;

    private Boolean successOn2xx;
}
