package uz.devops.pm.parser.payload.enumeration;

public enum RequestType {
    CHECK,
    PAY,
    STATUS,
    CANCEL,
    DEPOSIT,
    RECONCILE,
}
