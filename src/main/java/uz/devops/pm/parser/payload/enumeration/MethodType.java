package uz.devops.pm.parser.payload.enumeration;

public enum MethodType {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE,
}
