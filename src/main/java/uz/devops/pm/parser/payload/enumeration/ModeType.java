package uz.devops.pm.parser.payload.enumeration;

public enum ModeType {
    TEST,
    DEV,
    PROD,
}
