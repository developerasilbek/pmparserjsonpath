package uz.devops.pm.parser.service.impl;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONArray;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.devops.pm.parser.payload.PmParser;
import uz.devops.pm.parser.payload.enumeration.ModeType;
import uz.devops.pm.parser.payload.enumeration.ProtocolType;
import uz.devops.pm.parser.service.PmParserService;
import uz.devops.pm.parser.service.command.CommandContainer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
@Service(PmParserService.NAME)
@ConditionalOnProperty(
    prefix = "pm-parser",
    name = "simulate",
    havingValue = "false"
)
public class PmParserServiceImpl implements PmParserService {

    @Value("${pm-structure.request}")
    private String requestJson;

    @Value("${pm-structure.response}")
    private String responseJson;

    @Value("${pm-structure.url}")
    private String url;

    @Value("${pm-structure.type}")
    private String type;

    @Value("${pm-structure.username}")
    private String username;

    @Value("${pm-structure.password}")
    private String password;

    @Value("${pm-structure.token}")
    private String token;

    @Value("${pm-structure.body}")
    private String body;

    @Value("${pm-structure.statusQuery}")
    private String statusQuery;

    @Value("${pm-structure.successCode}")
    private String successCode;

    @Value("${pm-structure.timeout}")
    private String timeout;

    @Value("${pm-structure.headers}")
    private String headers;

    @Value("${pm-structure.httpMethodType}")
    private String httpMethodType;

    @Value("${pm-structure.auth.type}")
    private String authType;

    @Value("${pm-structure.protocol}")
    private String protocol;

    @Value("${pm-structure.requestMediaType}")
    private String requestMediaType;

    @Value("${pm-structure.responseMediaType}")
    private String responseMediaType;

    @Value("${pm-structure.active}")
    private Boolean active;

    @Value("${pm-structure.mode}")
    private String mode;

    @Value("${pm-structure.numOfPackage}")
    private String packages;

    @Value("${pm-structure.extIdQuery}")
    private String extIdQuery;

    @Value("${pm-structure.successOn2xx}")
    private String successOn2xx;

    @Value("${pm-structure.dataQueries}")
    private String dataQueries;

    @Value("${pm-structure.callback}")
    private String callback;

    @Value("${pm-structure.messageQuery}")
    private String messageQuery;

    private final CommandContainer commandContainer;

    public PmParserServiceImpl(CommandContainer commandContainer) {
        this.commandContainer = commandContainer;
    }

    @Override
    public List<PmParser> parse(MultipartFile file) throws IOException {
        log.info("Request for parse postman-collection");
        List<PmParser> pmParserList = new ArrayList<>();

        DocumentContext ctx = JsonPath.parse(file.getInputStream());

        Integer numOfPackage = ctx.read(packages);
        List<Integer> numOfItemsList = new ArrayList<>();
        for (int i = 0; i < numOfPackage; i++) {
            numOfItemsList.add(ctx.read("$.item[" + i + "].item.length()"));
        }

        int i = 0;
        for (Integer numberOfItems : numOfItemsList) {
            for (int j = 0; j < numberOfItems; j++) {
                PmParser pmParser = new PmParser();

                //default values
                pmParser.setProtocol(ProtocolType.valueOf(protocol));
                pmParser.setRequestMediaType(requestMediaType);
                pmParser.setResponseMediaType(responseMediaType);
                pmParser.setActive(active);
                pmParser.setMode(ModeType.valueOf(mode));

                String currectItem = "$.item[" + i + "].item[" + j + "]";
                HashMap<String, String> request = ctx.read(currectItem + requestJson);
                JSONArray response = ctx.read(currectItem + responseJson);

                commandContainer.getCommand("body").execude(request, ctx, pmParser, currectItem, body);
                commandContainer.getCommand("url").execude(request, ctx, pmParser, currectItem, url);
                commandContainer.getCommand("method").execude(request, ctx, pmParser, currectItem, httpMethodType);
                commandContainer.getCommand("header").execude(request, ctx, pmParser, currectItem, headers);
                commandContainer.getCommand("username").execude(request, ctx, pmParser, currectItem, username);
                commandContainer.getCommand("password").execude(request, ctx, pmParser, currectItem, password);
                commandContainer.getCommand("token").execude(request, ctx, pmParser, currectItem, token);

                if (!response.isEmpty()) {
                    commandContainer.getCommand("statusQuery").execude(request, ctx, pmParser, currectItem, statusQuery);
                    commandContainer.getCommand("successCode").execude(request, ctx, pmParser, currectItem, successCode);
                    commandContainer.getCommand("timeout").execude(request, ctx, pmParser, currectItem, timeout);
                }

                pmParserList.add(pmParser);
            }
            i++;
        }

        return pmParserList;
    }

    @Override
    public Boolean checkFile(MultipartFile file) {
        String originalFilename = file.getOriginalFilename();
        if (file.isEmpty()) {
            log.warn("File is null : {}", file);
            return false;
        }
        if (originalFilename == null || !originalFilename.endsWith(".postman_collection.json")) {
            log.warn("File is not postman collection");
            return false;
        }
        return true;
    }
}
