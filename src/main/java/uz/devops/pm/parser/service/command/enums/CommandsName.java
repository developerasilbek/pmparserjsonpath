package uz.devops.pm.parser.service.command.enums;

import lombok.Getter;

@Getter
public enum CommandsName {
    COMMAND_BODY("body"),
    COMMAND_URL("url"),
    COMMAND_METHOD("method"),
    COMMAND_HEADER("header"),
    COMMAND_USERNAME("username"),
    COMMAND_PASSWORD("password"),
    COMMAND_TOKEN("token"),
    COMMAND_STATUS_QUERY("statusQuery"),
    COMMAND_SUCCESS_CODE("successCode"),
    COMMAND_TIMEOUT("timeout");

    private final String commandName;

    CommandsName(String commandName) {
        this.commandName = commandName;
    }
}
