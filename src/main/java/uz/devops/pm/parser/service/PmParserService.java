package uz.devops.pm.parser.service;

import org.springframework.web.multipart.MultipartFile;
import uz.devops.pm.parser.payload.PmParser;

import java.io.IOException;
import java.util.List;

public interface PmParserService {

    String NAME = "pmParserService";

    List<PmParser> parse(MultipartFile file) throws IOException;

    Boolean checkFile(MultipartFile file);

}
