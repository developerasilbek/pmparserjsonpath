package uz.devops.pm.parser.service.command;

import com.jayway.jsonpath.ReadContext;
import uz.devops.pm.parser.payload.PmParser;

import java.util.HashMap;

public interface ExecudeCommand {
    void execude(HashMap<String, String> request, ReadContext ctx, PmParser pmParser, String currentItem, String value);

    String commandName();
}
