package uz.devops.pm.parser.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.devops.pm.parser.payload.PmParser;
import uz.devops.pm.parser.service.PmParserService;

import java.io.IOException;
import java.util.List;

@Slf4j
@Service(PmParserService.NAME)
@ConditionalOnProperty(
    prefix = "pm-parser",
    name = "simulate",
    havingValue = "true",
    matchIfMissing = true
)
public class DummyPmParserServiceImpl implements PmParserService {

    public DummyPmParserServiceImpl() {
        log.debug("############### PmParser simulation is ON ###############");
    }

    @Override
    public List<PmParser> parse(MultipartFile file) throws IOException {
        return null;
    }

    @Override
    public Boolean checkFile(MultipartFile file) {
        return null;
    }
}
