package uz.devops.pm.parser.service.command.impl;

import com.jayway.jsonpath.ReadContext;
import org.springframework.stereotype.Service;
import uz.devops.pm.parser.payload.PmParser;
import uz.devops.pm.parser.service.command.ExecudeCommand;
import uz.devops.pm.parser.service.command.enums.CommandsName;

import java.util.HashMap;

@Service
public class CommandHeader implements ExecudeCommand {
    @Override
    public void execude(HashMap<String, String> request, ReadContext ctx, PmParser pmParser, String currentItem, String value) {
        if (request.containsKey(CommandsName.COMMAND_HEADER.getCommandName())) {
            pmParser.setHeaders(ctx.read(currentItem + value).toString());
        }
    }

    @Override
    public String commandName() {
        return CommandsName.COMMAND_HEADER.getCommandName();
    }
}
