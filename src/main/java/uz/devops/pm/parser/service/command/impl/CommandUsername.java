package uz.devops.pm.parser.service.command.impl;

import com.jayway.jsonpath.ReadContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import uz.devops.pm.parser.payload.PmParser;
import uz.devops.pm.parser.service.command.ExecudeCommand;
import uz.devops.pm.parser.service.command.enums.CommandsName;

import java.util.HashMap;

@Service
public class CommandUsername implements ExecudeCommand {
    @Value("${pm-structure.auth.type}")
    private String authType;

    @Override
    public void execude(HashMap<String, String> request, ReadContext ctx, PmParser pmParser, String currentItem, String value) {
        if (request.containsKey("auth") && ctx.read(currentItem + authType).equals("basic")) {
            pmParser.setUsername(ctx.read(currentItem + value));
        }
    }

    @Override
    public String commandName() {
        return CommandsName.COMMAND_USERNAME.getCommandName();
    }
}
