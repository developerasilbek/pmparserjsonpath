package uz.devops.pm.parser.service.command.impl;

import com.jayway.jsonpath.ReadContext;
import org.springframework.stereotype.Service;
import uz.devops.pm.parser.payload.PmParser;
import uz.devops.pm.parser.payload.enumeration.MethodType;
import uz.devops.pm.parser.service.command.ExecudeCommand;
import uz.devops.pm.parser.service.command.enums.CommandsName;

import java.util.HashMap;

@Service
public class CommandMethod implements ExecudeCommand {
    @Override
    public void execude(HashMap<String, String> request, ReadContext ctx, PmParser pmParser, String currentItem, String value) {
        if (request.containsKey(CommandsName.COMMAND_METHOD.getCommandName())) {
            pmParser.setHttpMethodType(MethodType.valueOf(ctx.read(currentItem + value)));
        }
    }

    @Override
    public String commandName() {
        return CommandsName.COMMAND_METHOD.getCommandName();
    }
}
