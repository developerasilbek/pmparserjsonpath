package uz.devops.pm.parser.service.command;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CommandContainer {
    private final Map<String, ExecudeCommand> commandMap = new HashMap<>();

    public CommandContainer(List<ExecudeCommand> commands) {
        commands.forEach(execudeCommand -> commandMap.put(execudeCommand.commandName(), execudeCommand));
    }

    public ExecudeCommand getCommand(String commandName) {
        return commandMap.get(commandName);
    }
}
